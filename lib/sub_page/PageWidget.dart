import 'package:counter_app/screen/HomePage.dart';
import 'package:flutter/material.dart';

class SubPageWidget extends StatefulWidget{
  final ValueChanged<bool> parentAction;

  SubPageWidget({Key key, this.parentAction}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return SubPageWidgetState();
  }

}
class SubPageWidgetState extends State<SubPageWidget> with AutomaticKeepAliveClientMixin<SubPageWidget>{
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            '$_counter',
              style: TextStyle(fontSize: 40, color: Colors.blue)
          ),
          RaisedButton(
            onPressed: incrementCounter,
            textColor: Colors.white,
            color: Colors.blue,
            padding: const EdgeInsets.all(8.0),
            child: Text('Increment me!', style: TextStyle(fontSize: 20)),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;


//increment counter when pressed increment me button
  void incrementCounter() {
    setState(() {
      _counter = _counter + 1;
    });

    bool result = (_counter % 2) == 0 ? true : false;
    widget.parentAction(result);
  }

  //increment counter when pressed floating action button from parent widget
  void incrementCounterFromParentWidget() {
    setState(() {
      _counter = _counter + 1;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

}