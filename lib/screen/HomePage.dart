import 'package:flutter/material.dart';
import 'package:counter_app/sub_page/PageWidget.dart';


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage>  with SingleTickerProviderStateMixin{
  GlobalKey<SubPageWidgetState> _keySubpage1 = GlobalKey(), _keySubpage2 = GlobalKey(), _keySubpage3 = GlobalKey();
  int _selectedIndex = 0;
  bool isFloatingButtonNeeded = true;
  PageController pageController = PageController(
    initialPage: 0,
    keepPage: true,
  );

  //its called when counter is updated from sub-pages
  void onUpdateCounter(bool result) {
    setState(() {
      isFloatingButtonNeeded = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter $_selectedIndex"),
      ),
      body: buildPageView(),
      bottomNavigationBar: buildBottomNavigationBar(),
      floatingActionButton: new Visibility(
        visible: isFloatingButtonNeeded,
        child: FloatingActionButton(
              onPressed: (){
                if(_selectedIndex == 1) {
                  _keySubpage2.currentState.incrementCounterFromParentWidget();
                }else if(_selectedIndex == 2) {
                  _keySubpage3.currentState.incrementCounterFromParentWidget();
                }else{
                  _keySubpage1.currentState.incrementCounterFromParentWidget();
                }
              },
              tooltip: 'Increment',
              child: Icon(Icons.add),
            )
        ),
    );
  }

  //build page view for showing bottom navigation item pages content
  Widget buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        _onItemTapped(index);
      },
      children: <Widget>[
        SubPageWidget(key: _keySubpage1, parentAction: onUpdateCounter,),
        SubPageWidget(key: _keySubpage2, parentAction: onUpdateCounter),
        SubPageWidget(key: _keySubpage3, parentAction: onUpdateCounter),
      ],
    );
  }


  //build bottom navigation bar widget
  Widget buildBottomNavigationBar(){
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.lock_clock),
          label: 'Counter 1',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.watch),
          label: 'Counter 2',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.countertops_rounded),
          label: 'Counter 3',
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: Colors.blue,
      onTap:(index) {
        _onItemTapped(index);
        // pageController.jumpToPage(_selectedIndex);
        pageController.animateToPage(_selectedIndex, curve: Curves.ease, duration: Duration(milliseconds: 200));
      },
    );
  }

  //when tapped on bottom navigation bar item and switch pageview component
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }
}
